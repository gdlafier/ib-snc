var express = require('express');
var config = require('../config.js');
var router = express.Router();
var jwt = require('jsonwebtoken');
var supersecretword = "flex";
/*Validate User */
router.get('/', function(req, res, next) {
  var up = req.headers.authorization;
  if(!up){
    return res.status(401).send("Unauthorized");
  }
  //console.log("aaa")
  userpwd = up.split(' ')[1];
  var buf = Buffer.from(userpwd, 'base64').toString('ascii').split(":");
  var user = buf[0];
  var pass = buf[1];
  var payload = {};
  payload.user = user;
  payload.pass = pass;
  if(user == config.USER && pass== config.PASS){
    console.log("Authenticated");
    var token = jwt.sign(payload, supersecretword);
    res.status(200).json(token);
    //next();
  }else{
    res.status(401).send("Unauthorized");
  }

});

module.exports = router;
