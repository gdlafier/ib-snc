var express = require('express');
var router = express.Router();
var fs = require('fs');
var parseString = require('xml2js').parseString;


/* GET users listing. */
router.get('/', function(req, res, next) {
  var data = readFile('./user_list.xml');
  parseString(data, function (err, result) {
      res.send(result);
  });
});

router.post('/', function(req, res, next) {
  res.send('Post Users');
});

var readFile = function(file){
  return fs.readFileSync(file, 'utf8');
}

module.exports = router;
