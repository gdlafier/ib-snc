var express = require('express');
var config = require('../config.js');
var router = express.Router();
var supersecretword = "flex";
var jwt = require('jsonwebtoken');

/*Validate User */
router.use('/', function(req, res, next) {
  var token = req.headers['x-access-token'] || req.body.t || req.query.token;
  console.log(token)
  if(!token){
    return res.status(401).send("Unauthorized");
  }else{
    jwt.verify(token, supersecretword, function(err, decoded){
      if(err)
        res.status(500).json(err);
      next();
    });
  }

});

module.exports = router;
